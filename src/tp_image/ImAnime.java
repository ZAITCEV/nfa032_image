package tp_image;



/**
 * 
 * class qui garde le tableau d'images: Image[] image
 * l'image se modifit avec une frequence: int frequenceForChange
 * 
 * @author Kvazibus
 *
 */
public class ImAnime extends Image {

	public Image[] image;
	private int courant;
	public int n;
	public int max;
	public int event;
	private int percentOpacity;
	private int comptOpacity;

	public ImAnime(Image[] image, int frequenceForChange) {
		super(image [0]);
		this.image = image;
		this.courant = 0;
		this.n = 0;
		this.max = frequenceForChange;
		this.event = 0;
		this.percentOpacity = 100;
		this.comptOpacity = 0;
	}
	public ImAnime(Image[] image, int frequenceForChange, int percentOpacity) {
		super(image[0]);		
		this.image = image;
		this.max = frequenceForChange;
		this.courant = 0;
		this.n = 0;
		this.event = 0;
		this.percentOpacity = percentOpacity;
		this.comptOpacity = 0;
	}

	public Image getImage() {
		n++;
		if (n >= max) {
			n = 0;
			courant++;
			if (courant>=image.length) {
				courant=0;
			}
		}
		return this.image[courant];
	}

	public void opacityChange() {
		n++;
		if (n >= max) {
			n = 0;
			comptOpacity +=percentOpacity;
			if (comptOpacity>=99) {
				this.copy(this.image[courant]);
				comptOpacity =0;
				courant++;
				if (courant>=image.length) {
					courant=0;
				}
			}else {
				this.summOpacity(this.image[courant], percentOpacity);
			}
		}
	}


}
