package tp_image;

public class ImageAIncruster extends Image {
	
	protected int x;
	protected int y;
	protected int xMin;
	protected int yMin;
	protected int xMax;
	protected int yMax;
	
	public ImageAIncruster(Pixel[][] pixel, int xStart, int yStart, int xMin, int yMin, int xMax, int yMax) {
		super(pixel);
		this.x = xStart;
		this.y = yStart;
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
	}
	
	public ImageAIncruster() {
		Pixel[][] pixel = new Pixel[0][0];
		this.pixel = pixel;
		this.x = 0;
		this.y = 0;
		this.xMin = 0;
		this.yMin = 0;
		this.xMax = 0;
		this.yMax = 0;		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getxMin() {
		return xMin;
	}

	public void setxMin(int xMin) {
		this.xMin = xMin;
	}

	public int getyMin() {
		return yMin;
	}

	public void setyMin(int yMin) {
		this.yMin = yMin;
	}

	public int getxMax() {
		return xMax;
	}

	public void setxMax(int xMax) {
		this.xMax = xMax;
	}

	public int getyMax() {
		return yMax;
	}

	public void setyMax(int yMax) {
		this.yMax = yMax;
	}
	

}
