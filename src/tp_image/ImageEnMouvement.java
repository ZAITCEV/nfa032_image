package tp_image;

/**
 * @author ZAITECV 1. dx, dy - deplacement d'objet
 * 
 *         2. aleatoire = true - si deplacement en aleatoire,
 * 
 *         3. compteur - servir la frequance de changement la direction d'objet
 *         (haut/bas/gauche/droite) compteur - est une variable pour compter le
 *         temps pendant la phase courant
 * 
 *         4. phase = true - deplacement possible => mouvement r�alis� phase
 *         =false - deplacement en phahse "stop"
 * 
 *         5. oldX et oldY - garder les X et Y de old frames pour corriger les
 *         circonstances des objets
 * 
 *         6. predateurEtat - indice de l'Etat d'un objet: predateurEtat = 0 :
 *         victime vivant predateurEtat = -1 : victime est capt�e predateurEtat
 *         = -10: victime est disparue predateurEtat = 1 : predateur chasse
 *         predateurEtat = 2 : predateur revient au f�ret predateurEtat = 3 :
 *         predateur dort
 */
public class ImageEnMouvement extends ImageAIncruster implements Mouvements {

	private int dx;
	private int dy;
	private int oldX;
	private int oldY;
	private boolean aleatoire = true;
	private int compteur;
	private boolean phase;
	private int tempsStop = 75;
	private int predateurEtat = 0;
	private ImageEnMouvement victimeDePredateur = null;

	public ImageEnMouvement(int largeur, int hauteur, int x, int y, int xMin, int yMin, int xMax, int yMax, int dx,
			int dy) {
		Pixel[][] newPix = new Pixel[largeur][hauteur];
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				newPix[i][j] = new Pixel();
			}
		}
		this.filename = "none";
		this.bw = false;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.aficher = null;
		this.x = x;
		this.y = y;
		this.oldX = x;
		this.oldY = y;
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
		this.dx = dx;
		this.dy = dy;
	}

	public ImageEnMouvement(String path, String file, int x, int y, int xMin, int yMin, int xMax, int yMax, int dx,
			int dy) {
		this.pixel = ImageUtil.lireFichier(path + file);
		this.filename = file;
		this.bw = false;
		this.largeur = this.pixel.length;
		this.hauteur = this.pixel[0].length;
		this.aficher = null;
		this.x = x;
		this.y = y;
		this.oldX = x;
		this.oldY = y;
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
		this.dx = dx;
		this.dy = dy;
		this.aleatoire = false;
		this.compteur = 0;
		this.phase = true;

	}

	public ImageEnMouvement(String path, String file, int x, int y, int xMin, int yMin, int xMax, int yMax, int dx,
			int dy, boolean aleatoire) {
		this.pixel = ImageUtil.lireFichier(path + file);
		this.filename = file;
		this.bw = false;
		this.largeur = this.pixel.length;
		this.hauteur = this.pixel[0].length;
		this.aficher = null;
		this.x = x;
		this.y = y;
		this.oldX = x;
		this.oldY = y;
		this.xMin = xMin;
		this.yMin = yMin;
		this.xMax = xMax;
		this.yMax = yMax;
		this.dx = dx;
		this.dy = dy;
		this.aleatoire = aleatoire;
		this.compteur = 0;
	}

	public void setPredateur(int etat) {
		predateurEtat = etat;
	}

	@Override
	public void deplacementX() {
		x += dx;
		if (x + this.getLargeur() > xMax) {
			this.miroir(false, true);
			this.dx = -this.dx;
			x = xMax - this.getLargeur() + 2 * dx;
		}
		if (x < xMin) {
			this.miroir(false, true);
			dx = -dx;
			x = xMin + 2 * dx;
		}
	}

	@Override
	public void deplacementY() {
		y += dy;
		if (y + this.getHauteur() > yMax) {
			dy = -dy;
			y = yMax - this.getHauteur() + 2 * dy;
		}
		if (y < yMin) {
			dy = -dy;
			y = yMin + 2 * dy;
		}
	}

	@Override
	public void deplacementXY() {
		oldX = x;
		oldY = y;
		x = x + dx;
		y = y + dy;
		if (x + this.getLargeur() > xMax) {
			this.miroir(false, true);
			this.dx = -this.dx;
			x = xMax - this.getLargeur() + 2 * dx;
		}
		if (x < xMin) {
			this.miroir(false, true);
			dx = -dx;
			x = xMin + 2 * dx;
		}
		if (y + this.getHauteur() > yMax) {
			dy = -dy;
			y = yMax - this.getHauteur() + 2 * dy;
		}
		if (y < yMin) {
			dy = -dy;
			y = yMin + 2 * dy;
		}
	}

	public void deplacementXYAleatoire() {
		if (aleatoire) {
			compteur++;
			randomeur();
			if (phase) {
				oldX = x;
				oldY = y;
				x = (int) (x + dx * (Math.random()));
				y = (int) (y + dy * (Math.random()));
				if (x + this.getLargeur() > xMax) {
					this.dx = -this.dx;
					x = x + 2 * dx;
					this.miroir(false, true);
				}
				if (x < xMin) {
					dx = -dx;
					x = x + 2 * dx;
					this.miroir(false, true);
				}
				if (y + this.getHauteur() > yMax) {
					dy = -dy;
					y = y + 2 * dy;
				}
				if (y < yMin) {
					dy = -dy;
					y = y + 2 * dy;
				}
			}
		}
	}

	public void deplXYAleatDepande(int parentXcenter, int parentYcenter, int delta) {
		if (aleatoire) {
			compteur++;
			randomeur();
			if (phase) {
				oldX = x;
				oldY = y;
				x = (int) (x + dx * (Math.random()));
				y = (int) (y + dy * (Math.random()));
				if (x + this.getLargeur() > xMax || x + this.getLargeur() > parentXcenter + delta) {
					this.miroir(false, true);
					this.dx = -this.dx;
					x = oldX + 2 * dx;
				}
				if (x < xMin && x < parentXcenter - delta) {
					this.miroir(false, true);
					dx = -dx;
					x = oldX + 2 * dx;
				}
				if (y + this.getHauteur() > yMax || y + this.getHauteur() > parentYcenter + delta) {
					dy = -dy;
					y = oldY + 2 * dy;
				}
				if (y < yMin && y < parentYcenter - delta) {
					dy = -dy;
					y = oldY + 2 * dy;
				}
			}
		}
	}

	private void randomeur() {
		int mouv = (int) (10 * Math.random());
		if (phase && mouv > 3 && mouv < 5) {
			phase = false;
			tempsStop = (int) (25 + 50 * Math.random());
		}
		if (compteur > tempsStop) {
			phase = true;
			tempsStop = (int) (25 + 50 * Math.random());
			compteur = 0;
			if (mouv < 1) {
				dx = -dx;
				this.miroir(false, true);
			}
			if (mouv > 9)
				dy = -dy;
		}
	}

	public void scaleImage(double scaleL, double scaleH) {
		Image newIm = new Image();
		newIm = this.scale(scaleL, scaleH);
		this.pixel = newIm.pixel;
		this.largeur = newIm.getLargeur();
		this.hauteur = newIm.getHauteur();
	}

	public int getOldX() {
		return this.oldX;
	}

	public int getOldY() {
		return this.oldY;
	}

	public void refletDeplacement() {
		x = oldX;
		y = oldY;
		this.miroir(false, true);
		dx = -dx;
		dy = -dy;
		compteur = 0;
	}

	// M�thode return true pour �lever le drapeauSupprimerObjet.
	// Le drapeauSupprimerObjet ne peut �lever que quand la victime aura
	// �t� mang�e dans la m�thode 'mangerVictime'
	public boolean deplacementPredateur(ImageEnMouvement[] imagesAnimes, int predateur) {
		boolean res = false;
		if (predateurEtat == 1) {
			chercherVictime(imagesAnimes, predateur);
		}
		if (predateurEtat == 2) {
			passerAuForet(imagesAnimes, predateur);
		}
		if (predateurEtat == 3) {
			res = mangerVictime(imagesAnimes, predateur);
		}
		return res;
	}

	private void chercherVictime(ImageEnMouvement[] imagesAnimes, int predateur) {
		if (imagesAnimes.length != 1) {
			ImageEnMouvement[] poulets = getTabVictimes(imagesAnimes, predateur);
			int[] victime = distanceMin(poulets, imagesAnimes[predateur]);
			int dx = victime[1];
			int dy = victime[2];
			if (dx > 150 || dy > 20) {
				imagesAnimes[predateur].dy = -dy / 80;
				imagesAnimes[predateur].dy = -dy / 20;
			} else {
				imagesAnimes[predateur].dx = -dx / 10;
				if (Math.abs(imagesAnimes[predateur].dx) < 7) {
					if (imagesAnimes[predateur].dx > 0) {
						imagesAnimes[predateur].dx = 10;
					} else {
						imagesAnimes[predateur].dx = -10;
					}
				}
				imagesAnimes[predateur].dy = -dy / 10;
				if (Math.abs(imagesAnimes[predateur].dy) < 3) {
					if (imagesAnimes[predateur].dy > 0) {
						imagesAnimes[predateur].dy = 4;
					} else {
						imagesAnimes[predateur].dy = -4;
					}
				}
			}
			this.deplacementXY();
		}
	}

	private ImageEnMouvement[] getTabVictimes(ImageEnMouvement[] imagesAnimes, int predateur) {
		ImageEnMouvement[] poulets = new ImageEnMouvement[imagesAnimes.length - 1];
		for (int im = 0, compt = 0; im < imagesAnimes.length; im++) {
			if (im != predateur) {
				poulets[compt] = imagesAnimes[im];
				compt++;
			}
		}
		return poulets;
	}

	private int[] distanceMin(ImageEnMouvement[] poulets, ImageEnMouvement renard) {
		int[] res = new int[3];
		int distanceMin = 1000, distance, dx, dy, xPoulet, yPoulet;
		int xRenard = renard.getXFace();
		int yRenard = renard.getY() + 10;

		for (int i = 0; i < poulets.length; i++) {
			xPoulet = poulets[i].getX() + poulets[i].getLargeur() / 2;
			yPoulet = poulets[i].getY() + poulets[i].getHauteur() / 2;
			dx = xRenard - xPoulet;
			dy = yRenard - yPoulet;
			distance = (int) Math.sqrt(Math.abs(dx * dx + dy * dy));
			if (distance < distanceMin) {
				res = new int[] { i, dx, dy };
			}
		}
		return res;
	}

	private int getXFace() {
		int xFace = this.getX();
		if (dx > 0)
			xFace += this.getLargeur();
		return xFace;
	}

	private void passerAuForet(ImageEnMouvement[] imagesAnimes, int predateur) {
		System.out.println("renard vient au f�ret");
		if (compteur == 0) {
			compteur++;
			int xRenard = imagesAnimes[predateur].getXFace();
			int yRenard = imagesAnimes[predateur].getY() + 10;
			this.dx = (int) (900 - xRenard) / 100;
			this.dy = (int) (450 - yRenard) / 100;
			this.deplacementXY();

			victimeDePredateur.dx = this.dx;
			victimeDePredateur.dy = this.dy;
			victimeDePredateur.deplacementXY();
		} else {
			if (imagesAnimes[predateur].getXFace() < 900) {
				this.deplacementXY();
				victimeDePredateur.deplacementXY();
			} else {
				predateurEtat = 3;
				compteur = 0;
				dx = 0;
				dy = 0;
			}
		}

	}

	// m�thode return true pour �lever le drapeauSupprimerObjet
	private boolean mangerVictime(ImageEnMouvement[] imagesAnimes, int predateur) {
		boolean res = false;
		compteur++;
		if (compteur < 50) {
			System.out.println("renard mange: " + compteur);
		} else {
			System.out.println("renard dort: " + compteur);
		}
		if (compteur == 50) {
			victimeDePredateur.predateurEtat = -10;
			res = true;
			this.scaleImage(1.1, 1.1);
		}
		if (compteur > 100) {
			compteur = 0;
			predateurEtat = 1;
			dx = -3;
			dy = -3;
			this.miroir(false, true);
		}
		return res;
	}

	public int getEtatPredateur() {
		return this.predateurEtat;
	}

	public void captureVictime(ImageEnMouvement imageEnMouvement) {
		victimeDePredateur = imageEnMouvement;
		victimeDePredateur.victimeDePredateur = this;
		this.miroir(false, true);
		compteur = 0;
		predateurEtat = 2;
		this.dx = 10;
		this.dy = 1;
		victimeDePredateur.x = (int) (this.x + this.largeur*0.85);
		victimeDePredateur.y = (int) (this.y + this.hauteur*0.2);
		victimeDePredateur.dx = this.dx;
		victimeDePredateur.dy = this.dy;
		victimeDePredateur.predateurEtat = -1;
		victimeDePredateur.aleatoire = false;
		victimeDePredateur.setxMax(this.getxMax());
		victimeDePredateur.setyMax(this.getyMax());
		this.deplacementXY();
		victimeDePredateur.deplacementXY();
//		System.out.println("renard x:" + x + " y:" + y + " dx" + dx + " dy" + dy);
//		System.out.println("poulet x:" + victimeDePredateur.x + " y:" + victimeDePredateur.y + " dx"
//				+ victimeDePredateur.dx + " dy" + victimeDePredateur.dy);
	}

}
