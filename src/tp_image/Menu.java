package tp_image;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
	static String workingDIR = System.getProperty("user.dir");
	static Image i1 = new Image();
	static String fileImage;
	static boolean i1null;
	static ArrayList<Image> alImage = new ArrayList<Image>();

	public static int menu(Scanner scan, String[] menu) {
		int res = -1;
		do {
			for (String m : menu)  System.out.println(m);
			System.out.print("votre choix: ");
			try {
				res = scan.nextInt();
				if (res < 0 || res >= menu.length)
					System.out.println("Erreur: votre choix doit etre compris entre 1 et " + (menu.length - 1));
			} catch (Exception e) {
				System.out.println("Erreur: votre choix doit etre nombre entier");
			}
		} while (res < 0 || res >= menu.length);
		return res;
	}

	public static int askNumber(Scanner scan, int min, int max, String text) {
		int res = 0;
		do {
			System.out.println(text);
			System.out.println("Entrez une nombre de " + min + " � " + max + " s.v.p.");
			System.out.print("votre choix: ");
			try {
				res = scan.nextInt();
				if (res < min || res > max)
					System.out.println("Erreur: votre choix doit etre compris entre " + min + " et " + max);
			} catch (Exception e) {
				System.out.println("Erreur: votre choix doit etre nombre entier");
			}
		} while (res < min || res > max);
		return res;
	}

	public static void changeName(String ad) {
		int ext = i1.getName().length() - 4; // filename sauf l'extension
		String fileImageNew = "";
		fileImageNew = fileImageNew + i1.getName().substring(0, ext) + ad + i1.getName().substring(ext);
		i1.setName(fileImageNew);
		System.out.println("Le nom d'image �t� chang� � " + i1.getName());
	}

	public static Image activationImage() {
		String[] names = new String[alImage.size()];
		for (int i = 0; i < alImage.size(); i++) {
			names[i] = i + "- " + (alImage.get(i).getName());
		}
		Scanner scan = new Scanner(System.in);
		int choix = menu(scan, names);
		return alImage.get(choix);
	}

	public static Image chargerImage(Image iNew) {
		String[] listImages = Image.voirDIR(workingDIR + "/src/images");
		String[] listImages2 = new String [listImages.length+1];
		for (int i=0; i<listImages.length; i++)
			listImages2 [i] = listImages[i];
		listImages2 [listImages2.length-1] = "0 - return";
		Scanner scan = new Scanner(System.in);
		int choixImage = menu(scan, listImages2);
		if (choixImage != 0) {
			fileImage = Image.nameFileFromMenu(choixImage - 1, listImages[choixImage - 1]);
			iNew = Image.fromFile(workingDIR + "/src/images/", fileImage);
			iNew.setName(fileImage);
			iNew.setBW(false);
			return iNew;
		}
		return null;
	}

	// ***************************
	// ***************************
	// MENU PRINCIPAL
	// ***************************
	// ***************************

	public static void main(String[] args) throws InterruptedException {
		String[] menuPrincipal = { "1- afficher l'image", "2- ouvrir un fichier image",
				"3- activer l'image t�l�charg�e", "4- modifier l'image", "5- incrustations",
				"6- sauver l'image dans un fichier", "7- animation simple, visualisation de TP6",
				"8- animation programm�e (projet de NFA032)", "0- quitter" };
		i1null = true;
		Scanner scan = new Scanner(System.in);
		int choix;
		do {
			System.out.println("*******************************************");
			System.out.println("----------   Menu  principal   ------------");
			if (!i1null)
				System.out.println("l'image active est " + i1.getName());
			System.out.println("\n");
			choix = menu(scan, menuPrincipal);
			if (choix == 1) {
				if (!i1null) {
					i1.afficherImage();
				} else
					System.out.println("Aucune image n'est pas en memoire. Il faut ouvrir un fichier image");
			}
			if (choix == 2) {
				if (!i1null)
					i1.fermerImage();
				i1 = chargerImage(i1);
				if (i1 != null) {
					i1null = false;
					alImage.add(i1);
					System.out.println("l'image " + i1.getName() + " est ouverte");
					i1.afficherImage();
				}
			}
			if (choix == 3) {
				i1.fermerImage();
				i1 = activationImage();
				i1.afficherImage();
			}
			if (choix == 4) {
				if (!i1null) {
					modificationImage();
				} else
					System.out.println("Aucune image n'est pas en memoire. Il faut ouvrir un fichier image");
			}
			if (choix == 5) {
				if (!i1null) {
					incrusterImage();
				} else
					System.out.println("Aucune image n'est pas en memoire. Il faut ouvrir un fichier image");
			}
			if (choix == 6) {
				if (!i1null) {
					i1.toFile(workingDIR + "/src/images/");
					System.out.println("l'image " + i1.getName() + " est sauvegard�e");
				} else
					System.out.println("Aucune image n'est pas en memoire. Il faut ouvrir un fichier image");
			}
			if (choix == 7) {
				if (!i1null)
					i1.fermerImage();
				Animation.animationSimple(workingDIR);
			}
			if (choix == 8) {
				Animation.animationProgramme(workingDIR);
			}

			if (choix == 0) {
				System.out.println("Merci et � bient�t");
			}
		} while (choix != 0);
		if (!i1null)
			i1.fermerImage();
	}

	/////////////////////////////
	/////////////////////////////
	// MODIFICATIONS Image
	/////////////////////////////
	/////////////////////////////

	public static void modificationImage() {
		String[] menuModif = { "1- afficher l'image", "2- transformer l'image en noir et blanc",
				"3- deformation horizontale", "4- deformation verticale", "5- deformation proportionelle",
				"6- inverser gauche <-> droite", "7- inverser haut <-> bas", "0- revenir au menu principal" };
		Scanner scan = new Scanner(System.in);
		int choix;
		do {
			System.out.println("*********************************************");
			System.out.println("Modification d'image " + i1.getName());
			choix = menu(scan, menuModif);
			if (choix == 1) {
				i1.afficherImage();
			}
			if (choix == 2) {
				i1.fermerImage();
				i1.toGrayScale();
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_bw");
				alImage.add(i1);
			}
			if (choix == 3) {
				int scaleInt = askNumber(scan, 0, 500, "Entrez l'�chele de transformation (en pourcentage)");
				double scale = scaleInt;
				i1.fermerImage();
				i1 = i1.scale(scale / 100, 1);
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transphorm�e.");
				changeName("_hor");
				alImage.add(i1);
			}
			if (choix == 4) {
				int scaleInt = askNumber(scan, 0, 500, "Entrez l'�chele de transformation (en pourcentage)");
				double scale = scaleInt;
				i1.fermerImage();
				i1 = i1.scale(1, scale / 100);
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_ver");
				alImage.add(i1);
			}
			if (choix == 5) {
				int scaleInt = askNumber(scan, 0, 500, "Entrez l'�chele de transformation (en pourcentage)");
				double scale = scaleInt;
				i1.fermerImage();
				i1 = i1.scale(scale / 100, scale / 100);
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_resize");
				alImage.add(i1);
			}
			if (choix == 6) {
				i1.fermerImage();
				i1.miroir(false, true);
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_mirH");
				alImage.add(i1);
			}
			if (choix == 7) {
				i1.fermerImage();
				i1.miroir(true, false);
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_mirV");
				alImage.add(i1);
			}
		} while (choix != 0);
	}

	/////////////////////////////
	/////////////////////////////
	// INCRUSTER Image
	/////////////////////////////
	/////////////////////////////
	public static void incrusterImage() {
		String[] menuModif = { "1- afficher l'image", "2- incruster image sur une autre image de disque d�r",
				"3- incruster image sur une autre image t�l�charg�e",
				"4- utiliser cette image comme le fon pour incruster une autre image de disque d�r",
				"5- utiliser cette image comme le fon pour incruster une autre image t�l�charg�",
				"0- revenir au menu principal" };
		Scanner scan = new Scanner(System.in);
		int choix;
		do {
			System.out.println("*********************************************");
			System.out.println("Modification d'image " + i1.getName());
			choix = menu(scan, menuModif);
			if (choix == 1) {
				i1.afficherImage();
			}
			if (choix == 2) {
				System.out.println("Procedure d'incrustration " + i1.getName());
				Image i2 = new Image();
				i2 = chargerImage(i2);
				System.out.println("l'image " + i2.getName() + " est ouvert" + "\n");

				int posHoriz = askNumber(scan, 0, i2.getLargeur() - 1, "Entrez la position horisontal du bord gauche");
				int posVert = askNumber(scan, 0, i2.getHauteur() - 1, "Entrez la position vertical du bord haut");
				i2.incruster(i1, posHoriz, posVert);
				i1.fermerImage();
				i1 = i2;
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_incr");
				alImage.add(i1);
			}
			if (choix == 3) {
				System.out.println("Procedure d'incrustration " + i1.getName());
				Image i2 = new Image();
				i2 = activationImage();
				System.out.println("l'image " + i2.getName() + " est activ�" + "\n");

				int posHoriz = askNumber(scan, 0, i2.getLargeur() - 1, "Entrez la position horisontal du bord gauche");
				int posVert = askNumber(scan, 0, i2.getHauteur() - 1, "Entrez la position vertical du bord haut");
				i2.incruster(i1, posHoriz, posVert);
				i1.fermerImage();
				i1 = i2;
				i1.afficherImage();
				System.out.println("l'image " + i1.getName() + " est transform�e.");
				changeName("_incr");
				alImage.add(i1);
			}
			if (choix == 4) {
				System.out.println("Procedure d'incrustration sur " + i1.getName());
				Image i2 = new Image();
				i2 = chargerImage(i2);
				System.out.println("l'image " + i2.getName() + " est ouvert" + "\n");

				int posHoriz = askNumber(scan, 0, i1.getLargeur() - 1, "Entrez la position horisontal du bord gauche");
				int posVert = askNumber(scan, 0, i1.getHauteur() - 1, "Entrez la position vertical du bord haut");
				i1.fermerImage();
				i1.incruster(i2, posHoriz, posVert);
				i1.afficherImage();
				changeName("_incr");
				alImage.add(i1);
			}
			if (choix == 5) {
				System.out.println("Procedure d'incrustration sur " + i1.getName());
				Image i2 = new Image();
				i2 = activationImage();
				System.out.println("l'image " + i2.getName() + " est activ�" + "\n");

				int posHoriz = askNumber(scan, 0, i1.getLargeur() - 1, "Entrez la position horisontal du bord gauche");
				int posVert = askNumber(scan, 0, i1.getHauteur() - 1, "Entrez la position vertical du bord haut");
				i1.fermerImage();
				i1.incruster(i2, posHoriz, posVert);
				i1.afficherImage();
				changeName("_incr");
				alImage.add(i1);
			}
		} while (choix != 0);
	}
}