package tp_image;

public class AnimationProgramme extends Animation {
	private ImageEnMouvement[] imagesAnimes;
	private boolean drapeuSupressionObjet = false;

	public AnimationProgramme(Image image, Image fond, ImageEnMouvement[] imagesAnimes) {
		super(image, fond);
		this.imagesAnimes = imagesAnimes;

	}

	public void animationProgramme() {
		ImageEnMouvement renard = imagesAnimes[0];
		ImageEnMouvement pouletJaune1 = imagesAnimes[1];
		ImageEnMouvement pouletJaune2 = imagesAnimes[2];
		ImageEnMouvement pouletBlanche = imagesAnimes[3];
		ImageEnMouvement poussin1 = imagesAnimes[4];
		ImageEnMouvement poussin2 = imagesAnimes[5];
		ImageEnMouvement poussin3 = imagesAnimes[6];
		renard.setPredateur(1);
		Image sc = new Image(this.scena);
		Image fon1 = new Image(this.fond);
		sc.ajouterFon(fon1, 0, 0);
		sc.incruster(poussin1, poussin1.getX(), poussin1.getY());
		sc.incruster(poussin2, poussin2.getX(), poussin2.getY());
		sc.incruster(poussin3, poussin3.getX(), poussin3.getY());
		sc.incruster(pouletBlanche, pouletBlanche.getX(), pouletBlanche.getY());
		sc.incruster(pouletJaune1, pouletJaune1.getX(), pouletJaune1.getY());
		sc.incruster(pouletJaune2, pouletJaune2.getX(), pouletJaune2.getY());
		sc.incruster(renard, renard.getX(), renard.getY());
		sc.afficherImage();
		int nf = 0;
		int framesTotal = 2000;
		boucleAnimation(renard, pouletJaune1, pouletJaune2, pouletBlanche, poussin1, poussin2, poussin3, sc, fon1, nf,
				framesTotal);

		System.out.println("C'est fin d'animation...");
		sc.fermerImage();
	}

	// 	boucle Principal de l'animation
	//	
	//
	private void boucleAnimation(ImageEnMouvement renard, ImageEnMouvement pouletJaune1, ImageEnMouvement pouletJaune2,
			ImageEnMouvement pouletBlanche, ImageEnMouvement poussin1, ImageEnMouvement poussin2,
			ImageEnMouvement poussin3, Image sc, Image fon1, int nf, int framesTotal) {
		while (nf < framesTotal) {
			nf++;
			sc.copy(this.scena);
			sc.ajouterFon(fon1, (int) (nf / 25), 130); // deplacement par X du fond 1fois sur 25 frames
			pouletJaune1.deplacementXYAleatoire();
			pouletJaune2.deplacementXYAleatoire();
			pouletBlanche.deplacementXYAleatoire();
			poussin1.deplXYAleatDepande(pouletBlanche.getX() + pouletBlanche.getLargeur() / 2,
					pouletBlanche.getY() + pouletBlanche.getHauteur() / 2, 80);
			poussin2.deplXYAleatDepande(pouletBlanche.getX() + pouletBlanche.getLargeur() / 2,
					pouletBlanche.getY() + pouletBlanche.getHauteur() / 2, 80);
			poussin3.deplXYAleatDepande(pouletBlanche.getX() + pouletBlanche.getLargeur() / 2,
					pouletBlanche.getY() + pouletBlanche.getHauteur() / 2, 80);
			drapeuSupressionObjet = renard.deplacementPredateur(imagesAnimes, 0);
			analyseDeplacementsObjets();
			if (poussin1.getEtatPredateur() != -10)		sc.incruster(poussin1, poussin1.getX(), poussin1.getY());
			if (poussin2.getEtatPredateur() != -10)		sc.incruster(poussin2, poussin2.getX(), poussin2.getY());
			if (poussin3.getEtatPredateur() != -10)		sc.incruster(poussin3, poussin3.getX(), poussin3.getY());
			if (pouletBlanche.getEtatPredateur() != -10) sc.incruster(pouletBlanche, pouletBlanche.getX(), pouletBlanche.getY());
			if (pouletJaune1.getEtatPredateur() != -10)	sc.incruster(pouletJaune1, pouletJaune1.getX(), pouletJaune1.getY());
			if (pouletJaune2.getEtatPredateur() != -10)	sc.incruster(pouletJaune2, pouletJaune2.getX(), pouletJaune2.getY());
			sc.incruster(renard, renard.getX(), renard.getY());
			sc.aficher.update(sc.pixel);
		}
	}

	private void analyseDeplacementsObjets() {
		if (this.drapeuSupressionObjet)
			supprimerObjet();
		int xRefBeg, xrefEnd, yRefBeg, yrefEnd, x1, y1, x2, y2;
		for (int imc = 0; imc < imagesAnimes.length - 1; imc++) {
			xRefBeg = imagesAnimes[imc].getX();
			xrefEnd = imagesAnimes[imc].getX() + imagesAnimes[imc].getLargeur();
			yRefBeg = imagesAnimes[imc].getY();
			yrefEnd = imagesAnimes[imc].getY() + imagesAnimes[imc].getHauteur();
			for (int im = imc + 1; im < imagesAnimes.length; im++) {
				x1 = imagesAnimes[im].getX();
				y1 = imagesAnimes[im].getY();
				x2 = imagesAnimes[im].getX() + imagesAnimes[im].getLargeur();
				y2 = imagesAnimes[im].getY() + imagesAnimes[im].getHauteur();
				if ((x1 < xrefEnd && x1 > xRefBeg && y1 < yrefEnd && y1 > yRefBeg)
						|| (x2 < xrefEnd && x2 > xRefBeg && y2 < yrefEnd && y2 > yRefBeg)) {
					if (imagesAnimes[imc].getEtatPredateur() > 0) {
						if (imagesAnimes[imc].getEtatPredateur() == 1) {
							imagesAnimes[imc].captureVictime(imagesAnimes[im]);
							System.out.println("renard a captur� " + imagesAnimes[im].getName());
						}
					} else {
						imagesAnimes[imc].refletDeplacement();
						imagesAnimes[im].refletDeplacement();
					}

				}
			}
		}
	}

	private void supprimerObjet() {
		int imc = 0;
		int delta = 0;
		ImageEnMouvement[] newImagesAnimes = new ImageEnMouvement[imagesAnimes.length - 1];
		while (imc < imagesAnimes.length) {
			if (imagesAnimes[imc].getEtatPredateur() != -10) {
				newImagesAnimes[imc - delta] = imagesAnimes[imc];
			} else {
//				System.out.println(imagesAnimes[imc].getName() + " a �t� supprim�");
				delta++;
			}
			imc++;
		}
		imagesAnimes = newImagesAnimes;

	}

}
