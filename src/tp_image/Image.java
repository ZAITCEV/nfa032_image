package tp_image;

import java.io.File;
import java.util.ArrayList;

public class Image {

	public Pixel[][] pixel;
	public int largeur;
	public int hauteur;
	public String filename;
	public boolean bw;
	public Afficheur aficher;

	public Image(Pixel[][] pixel) {
		this.largeur = pixel.length;
		this.hauteur = pixel[0].length;
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				this.pixel[i][j] = new Pixel();
			}
		}
		this.filename = "";
		this.bw = false;
		this.aficher = null;
	}

	public Image(int largeur, int hauteur) {
		this.pixel = new Pixel[largeur][hauteur];
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				this.pixel[i][j] = new Pixel();
			}
		}
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.filename = "new.png";
		this.bw = false;
		this.aficher = null;
	}

	public Image() {
		Pixel[][] pixel = new Pixel[0][0];
		this.pixel = pixel;
		this.hauteur = 0;
		this.largeur = 0;
		this.filename = "new.png";
		this.bw = false;
		this.aficher = null;
	}

	public Image(String path, String file) {
		this.pixel = ImageUtil.lireFichier(path + file);
		this.filename = file;
		this.bw = false;
		this.largeur = this.pixel.length;
		this.hauteur = this.pixel[0].length;
		this.aficher = null;
	}

	public Image(Image im) {
		this.pixel = new Pixel[im.largeur][im.hauteur];
		for (int i = 0; i < im.largeur; i++) {
			for (int j = 0; j < im.hauteur; j++) {
				this.pixel[i][j] = new Pixel();
				int r = im.pixel[i][j].getRouge();
				int g = im.pixel[i][j].getVert();
				int b = im.pixel[i][j].getBleu();
				this.pixel[i][j].setRGB(r, g, b);
				this.pixel[i][j].setAlpha(im.pixel[i][j].getAlpha());
			}
		}
		this.largeur = im.largeur;
		this.hauteur = im.hauteur;
		this.filename = im.filename;
		this.bw = im.bw;
		this.aficher = null;
	}

	public int getLargeur() {
		this.largeur = this.pixel.length;
		return this.largeur;
	}

	public int getHauteur() {
		this.hauteur = this.pixel[0].length;
		return this.hauteur;
	}

	public Pixel[][] getPixel() {
		return this.pixel;
	}

	public void setPixel(Pixel[][] pixel) {
		if (pixel != null) {
			this.pixel = pixel;
			this.largeur = pixel.length;
			this.hauteur = pixel[0].length;
		}
	}

	public String getName() {
		return this.filename;
	}

	public void setName(String filename) {
		this.filename = filename;
	}

	public boolean getBW() {
		return this.bw;
	}

	public void setBW(boolean b) {
		this.bw = b;
	}

	public String toString() {
		String res = "Largeur: " + this.largeur + "hauteur: " + this.hauteur + ", name: " + this.filename;
		return res;
	}

	public void toFile(String path) {
		ImageUtil.ecrireFichier(path + this.filename, this.pixel);
	}

	public static Image fromFile(String path, String file) {
		Image res = new Image(path, file);
		res.setName(file);
		res.largeur = res.pixel.length;
		res.hauteur = res.pixel[0].length;
		res.setBW(false);
		return res;
	}

	public void afficherImage() {
		this.aficher = new Afficheur(this.pixel);
	}

	public void fermerImage() {
		this.aficher.closeJFrame();
	}

	public Afficheur getJFrame() {
		return this.aficher;
	}

	public static String[] recevoirListDir(String path) {
		File catalog = new File(path);
		String[] files = catalog.list();
		File file;
		ArrayList<String> fileAL = new ArrayList<String>();
		for (String ff : files) {
			file = new File(catalog, ff);
			if (!ff.isEmpty() || file.isFile() || !file.isHidden() || file.canRead()) {
				fileAL.add(ff);
			}
		}
		String[] res = new String[fileAL.size()];
		for (int i = 0; i < res.length; i++) {
			res[i] = Integer.toString(i + 1) + " - " + fileAL.get(i);
		}
		return res;
	}

	public static String[] voirDIR(String workingDIR) {
		String[] images;
		images = recevoirListDir(workingDIR);
		return images;
	}

	public static String nameFileFromMenu(int i, String str) {
		int index = str.indexOf(" - ");
		String res = str.substring(index + 3);
		return res;
	}

	public void toGrayScale() {
		int larg = this.largeur;
		int haut = this.hauteur;
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				this.pixel[i][j].toGray();
			}
		}
		this.bw = true;
	}

	public void incruster(Image adImage, int shiftLargFon, int shiftHautFon) {
		int larg = adImage.getLargeur(); // determinaison les bords des images
		if (shiftLargFon < 0)
			shiftLargFon = 0; // pour ne pas depasser aux limites des Tabs
		if (shiftLargFon + larg >= this.largeur) {
			larg = this.largeur - shiftLargFon;
		}
		int haut = adImage.getHauteur();
		if (shiftHautFon < 0)
			shiftHautFon = 0;
		if (shiftHautFon + haut >= this.hauteur)
			haut = this.hauteur - shiftHautFon;
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				if (adImage.pixel[i][j].getAlpha() == 255) {
//					if (i >= adImage.largeur || j >=adImage.hauteur ||
//							shiftLargFon + i >= this.largeur || shiftHautFon + j >= this.hauteur
//							) {
//						System.out.println("i:" + i + ", j:" + j);
//						System.out.println(
//								"shiftHautFon+i:" + (shiftHautFon + i) + "   shiftLargFon+j: " + (shiftLargFon + j));
//						System.out.println("fon pixel: " + this.pixel[shiftLargFon + i][shiftHautFon + j].toString());
//						System.out.println("ad pixel: " + adImage.pixel[i][j].toString());
//					}
					this.pixel[shiftLargFon + i][shiftHautFon + j].summaryPixel(adImage.pixel[i][j]);
				}
			}
		}
		if (!adImage.bw) { // si l'image incrust� est en couleur
			this.bw = false; // on declare que le fon deviens en couleur
		}
	}

	public void ajouterFon(Image fon, int shiftLargFon, int shiftHautFon) {
		int larg = this.largeur;
		if (shiftLargFon < 0)
			shiftLargFon = 0; // pour ne pas depasser aux limites des Tabs
		if (shiftLargFon + larg >= fon.largeur)
			larg = fon.largeur - shiftLargFon;
		int haut = this.hauteur;
		if (shiftHautFon < 0)
			shiftHautFon = 0;
		if (shiftHautFon + haut >= fon.hauteur)
			haut = fon.hauteur - shiftHautFon;
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				if (this.pixel[i][j].getAlpha() == 0) {
					this.pixel[i][j].summaryPixel(fon.pixel[shiftLargFon + i][shiftHautFon + j]);
					this.pixel[i][j].setAlpha(255);
				}
			}
		}
	}

	public Image scale(double scaleL, double scaleH) {
		int largNew = (int) (this.largeur * scaleL);
		int hautNew = (int) (this.hauteur * scaleH);
		Image in = new Image(largNew, hautNew);
		for (int i = 0; i < largNew; i++) {
			for (int j = 0; j < hautNew; j++) {
				in.pixel[i][j] = this.pixel[(int) (i / scaleL)][(int) (j / scaleH)];
			}
		}
		in.setBW(this.bw);
		in.setName(this.getName());
		in.aficher = this.aficher;
		return in;
	}


//	mirroir horizontale => change   haut <-> bas
//	mirroir verticale   => change gauche <-> droite
	public void miroir(boolean mirroirHorizontale, boolean mirroirVerticale) {
		Pixel pxtemp = new Pixel();
		int larg = this.largeur;
		int haut = this.hauteur;
		int demilarg = larg / 2;
		int demihaut = haut / 2;
		if (mirroirHorizontale) {
			for (int l1 = 0; l1 < larg; l1++) {
				for (int h1 = 0, h2 = haut - 1; h1 < demihaut; h1++, h2--) {
					pxtemp = this.pixel[l1][h1];
					this.pixel[l1][h1] = this.pixel[l1][h2];
					this.pixel[l1][h2] = pxtemp;
				}
			}
		}
		if (mirroirVerticale) {
			for (int h = 0; h < haut; h++) {
				for (int l1 = 0, l2 = larg - 1; l1 < demilarg; l1++, l2--) {
					pxtemp = this.pixel[l1][h];
					this.pixel[l1][h] = this.pixel[l2][h];
					this.pixel[l2][h] = pxtemp;
				}
			}
		}
	}

	public Image clone() {
		int larg = this.largeur;
		int haut = this.hauteur;
		Image res = new Image(larg, haut);
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				res.pixel[i][j] = this.pixel[i][j];
			}
		}
		res.filename = this.filename;
		res.bw = this.bw;
		res.aficher = this.aficher;
		return res;
	}

	public void copy(Image im) {
		int larg = im.largeur;
		int haut = im.hauteur;
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				int r = im.pixel[i][j].getRouge();
				int g = im.pixel[i][j].getVert();
				int b = im.pixel[i][j].getBleu();
				this.pixel[i][j].setRGB(r, g, b);
				this.pixel[i][j].setAlpha(im.pixel[i][j].getAlpha());
			}
		}
	}

	public void summOpacity(Image adimage, int opacity) {
		int larg = adimage.largeur;
		int haut = adimage.hauteur;
		for (int i = 0; i < larg; i++) {
			for (int j = 0; j < haut; j++) {
				this.pixel[i][j].summOpacity(adimage.pixel[i][j], opacity);

			}
		}

	}

	public boolean equals(Image im) {
		boolean res = true, fin = false;
		int larg = im.largeur;
		int haut = im.hauteur;
		int i = 0, j = 0;
		while (res && !fin) {
			if (this.pixel[i][j].equals(im.pixel[i][j]))
				res = false;
			j++;
			if (j == haut) {
				j = 0;
				i++;
				if (i == larg) {
					fin = true;
				}
			}
		}
		return res;
	}

}
