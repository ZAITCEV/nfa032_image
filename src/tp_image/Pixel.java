package tp_image;

public class Pixel {

	private int rouge;
	private int vert;
	private int bleu;
	private int alpha;

	
	public Pixel (int [] px) {
		this.rouge = px[0];
		this.vert = px[1];
		this.bleu = px[2];
		this.alpha = px[3];
	}
	public Pixel () {
		this.rouge = 0;
		this.vert = 0;
		this.bleu = 0;
		this.alpha = 0;
	}
	
	public Pixel(Pixel px) {
		this.rouge = px.getRouge();
		this.vert = px.getVert();
		this.bleu = px.getBleu();
		this.alpha = px.getAlpha();
	}
//	public Pixel(int rouge, int vert, int bleu, int alpha) {
//		this.rouge = rouge;
//		this.vert = vert;
//		this.bleu = bleu;
//		this.alpha = alpha;
//	}
	@Override
	public String toString() {
		return "Pixel [rouge=" + this.rouge + 
				", vert=" + this.vert + 
				", bleu=" + this.bleu + 
				", alpha=" + this.alpha + "]";
	}
	public int getRouge() {
		return rouge;
	}
	public void setRouge(int rouge) {
		this.rouge = rouge;
	}
	public int getVert() {
		return vert;
	}
	public void setVert(int vert) {
		this.vert = vert;
	}
	public int getBleu() {
		return bleu;
	}
	public void setBleu(int bleu) {
		this.bleu = bleu;
	}
	public int getAlpha() {
		return alpha;
	}
	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}

	public int [] getPixel() {
		int [] px = new int [4];
		px[0] = this.rouge;
		px[1] = this.vert ;
		px[2] = this.bleu ;
		px[3] = this.alpha;
		return px;
	}
	public void setRGB (int r,int g,int b) {
		this.rouge=r;
		this.vert=g;
		this.bleu=b;
	}

	public void toGray () {
		this.rouge=(int) (this.rouge*0.33+this.vert*0.33+this.bleu*0.33);
		this.vert=this.rouge;
		this.bleu=this.vert;
	}
	public void summaryPixel(Pixel px) {
		int r=px.getRouge();
		int g=px.getVert();
		int b=px.getBleu();
		this.setRouge(r);
		this.setVert(g);
		this.setBleu(b);
//		this.setAlpha(px.getAlpha());
	}
	public boolean equals (Pixel p) {
		if (this.rouge==p.rouge &&
				this.vert == p.vert &&
				this.bleu == p.bleu &&
				this.alpha == p.alpha) {
				return true;
			}
		return false;
	}
	
	public void summOpacity(Pixel pixel, int opacity) {
		this.rouge= (int) (this.rouge*(1-opacity/100)+pixel.rouge*opacity/100);
		this.vert= (int) (this.vert*(1-opacity/100)+pixel.vert*opacity/100);
		this.bleu= (int) (this.bleu*(1-opacity/100)+pixel.bleu*opacity/100);
		
	}
	public static void main(String [] args) {
		int [] px1= new int [] {255,0,0,255};
		Pixel p1 = new Pixel(px1);
		Pixel p2 = new Pixel();
		System.out.println ("Pixel 1: "+p1.toString());
		System.out.println ("Pixel 2: "+p2.toString());
		p1.setBleu(135);
		p1.setVert(135);
		p1.setRouge(135);
		p2.setBleu(255);
		p2.setVert(255);
		p2.setRouge(0);
		System.out.println ("Pixel 1: "+p1.toString());
		System.out.println ("Pixel 2: "+p2.toString());
		
	}
}
