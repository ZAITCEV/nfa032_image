package tp_image;

public class Animation {

	protected Image scena;
	protected Image fond;
//	private ImAnime fon;

	// c'est un constructeur qui contient le fond et le scene pour animation
	public Animation(Image image, Image fond) {
		this.scena = image;
		this.fond = fond;
	}

	public void AnimationXY(Image i, int xstart, int ystart, int xfinish, int yfinish, int stepX, int stepY) {
		scena.afficherImage();
		for (int x = xstart, y = ystart; x <= xfinish; x = x + stepX, y = y + stepY) {
			scena.copy(this.fond);
			scena.incruster(i, x, y);
			scena.aficher.update(scena.pixel);
		}
	}

	public static void animationSimple(String workingDIR) throws InterruptedException {
		Image sky10 = new Image(workingDIR + "/src/imagesanim/", "sky10.png");
		Image cottage = new Image(workingDIR + "/src/imagesanim/", "cottage3.png");

		// je declare, que l'Image "cottage" est une scene de l'animation
		Animation anim = new Animation(cottage, sky10);

		// je limite l'espace pour deplacer l'avion:
		// x: 0...cottage.getLargeur()-200
		// y: 0...cottage.getHauteur()/3
		ImageEnMouvement avion = new ImageEnMouvement(workingDIR + "/src/imagesanim/", "avion.png", 20, 30, 0, 0,
				cottage.getLargeur() - 200, cottage.getHauteur() / 3, 6, -1);
		avion.scaleImage(0.3, 0.3);
		anim.animeSimple(avion);
	}

	public void animeSimple(ImageEnMouvement avion) throws InterruptedException {
		Image sc = new Image(this.scena);
		Image fon1 = new Image(this.fond);
		sc.ajouterFon(fon1, 0, 0);
		sc.afficherImage();
		int nf = 0;
		int framesTotal = 500;
		while (nf < framesTotal) {
			nf++;
			sc.copy(this.scena);
			System.out.println("frame= " + nf);
			sc.ajouterFon(fon1, (int) (nf / 25), 130);
			avion.deplacementXY();
			sc.incruster(avion, avion.getX(), avion.getY());
			sc.aficher.update(sc.pixel);
			Thread.sleep(15);
		}

		System.out.println("The end...");
		sc.fermerImage();
	}

	public static void animationProgramme(String workingDIR) throws InterruptedException {
		Image sky10 = new Image(workingDIR + "/src/imagesanim/", "sky10.png");
		Image cottage = new Image(workingDIR + "/src/imagesanim/", "cottage3.png");
		int sceneLarg = cottage.getLargeur();
		int sceneHaut = cottage.getHauteur();
		ImageEnMouvement renard = new ImageEnMouvement(workingDIR + "/src/images/", "renard-petit.png", sceneLarg - 220,sceneHaut - 150, 0, sceneHaut / 2, sceneLarg, sceneHaut, -5, -3);
		ImageEnMouvement pouletJaune = new ImageEnMouvement(workingDIR + "/src/images/", "pouletJaune.png", 220, 430,30, 420, 400, sceneHaut - 20, 5, 3, true);
		pouletJaune.scaleImage(0.3, 0.3);
		pouletJaune.miroir(false, true);
		ImageEnMouvement pouletJaune2 = new ImageEnMouvement(workingDIR + "/src/images/", "pouletJaune.png", 220, 580,30, 420, 400, sceneHaut - 20, 5, 3, true);
		pouletJaune2.scaleImage(0.3, 0.3);
		pouletJaune2.miroir(false, true);
		ImageEnMouvement pouletBlanche = new ImageEnMouvement(workingDIR + "/src/images/", "pouletBlanche.png", 120, 630, 30, 420, 400, sceneHaut - 20, 3, 2, true);
		pouletBlanche.scaleImage(0.25, 0.25);
		pouletBlanche.miroir(false, true);
		ImageEnMouvement poussin1 = new ImageEnMouvement(workingDIR + "/src/images/", "poussin-2.png", 260, 530, 30,420, 400, sceneHaut - 20, -4, -3, true);
		poussin1.scaleImage(0.15, 0.15);
		ImageEnMouvement poussin2 = new ImageEnMouvement(workingDIR + "/src/images/", "poussin-2.png", 360, 490, 30,420, 400, sceneHaut - 20, -4, -3, true);
		poussin2.scaleImage(0.15, 0.15);
		ImageEnMouvement poussin3 = new ImageEnMouvement(workingDIR + "/src/images/", "poussin-2.png", 160, 490, 30,420, 400, sceneHaut - 20, -4, -3, true);
		poussin3.scaleImage(0.15, 0.15);
		ImageEnMouvement [] imagesAnimes = new ImageEnMouvement [] {renard, pouletJaune, pouletJaune2, pouletBlanche, poussin1, poussin2, poussin3};
		AnimationProgramme anim = new AnimationProgramme(cottage, sky10, imagesAnimes);
		anim.animationProgramme();
	}
}
